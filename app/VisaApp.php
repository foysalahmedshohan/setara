<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisaApp extends Model
{
    // Mass Asignment
    protected $guarded = [];

    public function visatype()
    {
        return $this->belongsTo('App\VisaType','visa_type');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }



    public function paytype()
    {
        return $this->belongsTo('App\PaymentType', 'id');
    }

    public function clients()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }
}
