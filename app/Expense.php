<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    // Mass Asignment
    protected $guarded = [];

    public function exname()
    {
        return $this->belongsTo('App\ExpenseName', 'expense_id');
    }

    public function income()
    {
        return $this->belongsToMany('App\Invoice');
    }

    public function agents()
    {
        return $this->belongsTo('App\Agent','agent_id');
    }

     public function invoice()
    {
        return $this->belongsTo('App\Invoice','invoice_id');
    }
}
