<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    // Mass Asignment
    protected $guarded = [];


    public function packages(){
        return $this->hasMany('App\Invoice');
    }
}
