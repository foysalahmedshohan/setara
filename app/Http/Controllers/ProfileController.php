<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Profile;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::get();
       // return  $profile;
        $title = 'Company Profile';
        $icon = 'mdi mdi-bank';
        return view('profile.index', compact('profiles','title','icon'))->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Company Profile';
        $icon = 'mdi mdi-bank';
        return view('profile.add', compact('title','icon'))->with('no', 1);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
   public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'tag' => 'nullable|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|max:255',
            'phone2' => 'required|max:255',
            'email' => 'required|max:255',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|nullable',
           
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $image = $request->file('photo');
        if($image != '') {
            $imageclient = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads\bank'), $imageclient);
            $form_data = array(
                'name' => $request->get('name'),
                'tag' => $request->get('tag'),
                'address' => $request->get('address'),
                'phone' => $request->get('phone'),
                'phone2' => $request->get('phone2'),
                'email' => $request->get('email'),
                'photo' => $imageclient
            );
        } else {
            $form_data = array(
               'name' => $request->get('name'),
                'tag' => $request->get('tag'),
                'address' => $request->get('address'),
                'phone' => $request->get('phone'),
                'phone2' => $request->get('phone2'),
                'email' => $request->get('email'),
              
            );
        }
        Profile::create($form_data);
        return redirect()->to('/companyProfile/create');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::find($id);
       // if (file_exists('image/'.$doctor->image)) {
       // // unlink('image/'.$doctor->image);

       //  }

        $profile->delete();

    }
}
