<?php

namespace App\Http\Controllers;
use App\validate;
use App\Income;
use App\Expense;
use App\Invoice;
use App\chartOfAccount;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $invoice = Income::all();
        $gtotal = Income::sum('pay_amount');
        $expense = Expense::where("status",1)->get();
        $etotal = Expense::where("status",1)->sum('amount');
        $title = 'Income Statement';
        $icon = 'mdi mdi-trophy-variant';
        return view('accounts.index', compact( 'invoice', 'expense', 'gtotal', 'etotal', 'title','icon'))->with('no', 1);
    }

   
    public function incomeSearch(Request $request) {
    	//$localtime = now();

    	$start = date("Y-m-d", strtotime($request->from_date));
    	$end = date("Y-m-d", strtotime($request->to_date));

       $invoice_search = Invoice::whereBetween('invoice_date', array($start, $end))
         ->get();
       $expense_search = Expense::whereBetween('ex_date', array($start, $end))
         ->get();
       $in_total = $invoice_search->sum('grand_total');

       $ex_total = $expense_search->sum('amount');

       $start_date = date("d-m-Y", strtotime($start));
    	$end_date = date("d-m-Y", strtotime($end));

       $title = 'Income Statement';
        $icon = 'mdi mdi-trophy-variant';

      return view('accounts.search', compact( 'invoice_search', 'expense_search', 'in_total', 'ex_total', 'start_date', 'end_date', 'title','icon'));
    }


    public function chartOfAccount(){
      $title = 'Account Statement Add';
      $icon = 'mdi mdi-trophy-variant';
      $root= chartOfAccount::where('rootId','0')->get();
      $parentId= chartOfAccount::where('rootId','0')->get();


      return view('accounts.chartOfAccount', compact('title','icon','root','parentId'));
    }


    public function chartOfAccountAdd(Request $request){

        //return $request->child_id;
        // $validator = $request->validate( [
        //     'title' => 'required|string',
        //     'rootId' => 'required',
        //     'parentId' => 'nullable|max:255',
        //     'accountCode' => 'nullable|max:255',
        //     'child_id' => 'nullable|max:255',
        //     'dist_id' => 'nullable|max:255',
        //     'status' => 'nullable|max:255',
        //     'common' => 'nullable',
        // ]);
        // if ($validator->fails()) {
        //     return redirect()->back()->withErrors($validator)->withInput();
        // }

      $id= chartOfAccount::where('rootId','0')->first(); 
      $root= chartOfAccount::where('rootId','0')->pluck("id");
      
      // /$count= chartOfAccount::where('rootId','0')->count();

      
      if ($id->rootId==0 && $request->title!=null && $request->parentId=='Select') 
      { 
          return 1;
         $rootAccId=$request->rootId;
         $AccParrentCount= chartOfAccount::where('rootId',$request->rootId)->count();
         $AccParrentCount= $AccParrentCount+1;

         $accountCode=$rootAccId.'-'.$AccParrentCount;
         $account= new chartOfAccount;
         $account->rootId=$rootAccId;
         $account->title=$request->title;
         $account->parentId=$request->parentId;
         $account->accountCode=$accountCode;
         $account->status=1;
         $account->dist_id=0;
         $account->notShow=0;
         $account->common=1;
         $account->save();
      }

      elseif ($id->rootId==0 && $request->parentId!='Select' &&  $request->child_id=='Select') 
      { 
         //for invoice code
         $rootAccId=$request->rootId;
         $parentId=$request->parentId;

         $AccParrentCount= chartOfAccount::where('parentId',$request->parentId)->count();
         $AccParrentCount= $AccParrentCount+1;

         $accountCode=$rootAccId.'-'.$parentId.'-'.$AccParrentCount;
         //return $accountCode;
         $account= new chartOfAccount;
         $account->rootId=$rootAccId;
         $account->title=$request->title;
         $account->parentId=$request->parentId;
         $account->accountCode=$accountCode;
         $account->status=1;
         $account->dist_id=0;
         $account->notShow=0;
         $account->common=1;
         $account->save();
      }

        elseif ($id->rootId==0 &&  $request->child_id!='Select') 
      { 
         $rootAccId=$request->rootId;
         $parentId=$request->parentId;
         $child_id=$request->child_id;

         $AccParrentCount= chartOfAccount::where('parentId',$request->child_id)->count();
         $AccParrentCount= $AccParrentCount+1;

         $accountCode=$rootAccId.'-'.$parentId.'-'.$child_id.'-'.$AccParrentCount;

         $account= new chartOfAccount;
         $account->rootId=$rootAccId;
         $account->title=$request->title;
         $account->parentId=$request->child_id;
         $account->accountCode=$accountCode;
         $account->status=1;
         $account->dist_id=0;
         $account->notShow=0;
         $account->common=1;
         $account->save();
      }



          return redirect()->to('/accounts/chartOfAccount');
    }


     public function parentID(Request $request)
    {
      $data = chartOfAccount::where("parentId",$request->id)->pluck("title","id");
      return response()->json($data);
    }

     public function child_id(Request $request)
    {
     $data = chartOfAccount::where("parentId",$request->id)->pluck("title","id");
     return response()->json($data);
    }

    

}
