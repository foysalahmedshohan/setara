<?php

namespace App\Http\Controllers;

use Validator;
use App\ExpenseName;

use Illuminate\Http\Request;

class ExpenseNameController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $expense_name = ExpenseName::all();
        $expense_id = ExpenseName::where("parent_id",0)->get();
        $expense = ExpenseName::select('*')->where('parent_id','=' , 0)->get();
        $sub_expense = ExpenseName::select('*')->where('parent_id', '!=' , 0)->get();
       
        $title = 'Expense Head';
        $icon = 'mdi mdi-cash';
        return view('setting.expense', compact('expense_id','expense_name','expense','sub_expense', 'title','icon'))->with('no', 1);
    }
    public function store(Request $request) {

        $expense_id = ExpenseName::where("ex_name",$request->ex_name)->first();
          if($expense_id!=null){
          $validator='This Expense Name Already Exist In Database';
          return redirect()->back()->withErrors($validator)->withInput();
          }

        $validator = Validator::make($request->all(), [
            'ex_name' => 'required|max:255',
            'parent_id' => 'nullable',
            'ex_desc' => 'nullable|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
            if ($request->parent_id == 'Select Sub Expense') 
            {
             ExpenseName::create([
            'ex_name' => $request->get('ex_name'),
            'parent_id' => 0,
            'ex_desc' => $request->get('ex_desc')
            ]);
            }
             
            else {
            ExpenseName::create([
            'ex_name' => $request->get('ex_name'),
            'parent_id' => $request->get('parent_id'),
            'ex_desc' => $request->get('ex_desc')
            ]);
            }
            
           
       
        return redirect()->to('/settings/exname');
    }

    public function delete($id) {
        $expense_name = ExpenseName::findOrFail($id);
        $expense_name->delete();
        return redirect()->to('/settings/exname');
    }

       public function sub_delete($id){
        $expense = ExpenseName::findOrFail($id);
        $expense->delete();
        return redirect()->to('/settings/exname');
    }
}
