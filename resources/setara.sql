-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2020 at 05:22 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `setara`
--

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upazila_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commission` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `name`, `agent_id`, `email`, `phone`, `phone2`, `agent_photo`, `address`, `district_id`, `upazila_id`, `commission`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Monir Hossain', '1', 'monir@gmail.com', '01987665433', '01987665432', 'monir-1579152704.jpg', 'Mirpur-1, Dhaka', '1', '12', 6, 1, '2020-01-15 23:31:44', '2020-01-15 23:35:33'),
(3, 'Ahmed', '5', 'shohan000@gmail.com', '01862547854', NULL, NULL, 'SHASAN GASA', '1', NULL, 10, 1, '2020-02-02 23:49:28', '2020-02-09 23:00:43'),
(4, 'Ahmed', '5', NULL, '01852458741', NULL, NULL, 'SHASAN GASA', '1', '12', 10, 1, '2020-02-03 01:16:57', '2020-02-03 01:16:57');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sing_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `op_balance` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `name`, `account_name`, `account_number`, `branch`, `sing_photo`, `op_balance`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Brac Bank', 'Base IT', '2553738929209727635', 'Mirpur-11', 'baseit-logo-1577692877.png', 10000, 1, '2019-12-30 02:01:17', '2019-12-30 02:01:17'),
(2, 'Eastern Bank Ltd', 'Base IT', '2553738929209727680', 'Mirpur-11', 'baseit-logo-1577692928.png', 1000, 1, '2019-12-30 02:02:08', '2019-12-30 02:02:08'),
(3, 'Dutch Bangla Bank', 'Base IT', '2553738929209727654', 'Mirpur-10', 'baseit-logo-1577692985.png', 5000, 1, '2019-12-30 02:03:05', '2019-12-30 02:03:05'),
(4, 'Social Islami Bank Limited', 'Base IT', '2553738929209766666', 'Mirpur-10', 'baseit-logo-1577693041.png', 3000, 1, '2019-12-30 02:04:01', '2019-12-30 02:04:01'),
(5, 'Dhaka Bank', 'shohan', '01658754125', 'mirpur branch', NULL, 50000, 1, '2020-02-03 01:32:20', '2020-02-03 01:32:20');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district_id` int(10) UNSIGNED NOT NULL,
  `upazila_id` int(10) UNSIGNED NOT NULL,
  `client_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport_expired` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport_issue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `em_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_insurance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_insurance_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_company_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `agent_id`, `email`, `phone`, `phone2`, `address`, `district_id`, `upazila_id`, `client_photo`, `passport_no`, `birth_date`, `passport_expired`, `passport_issue`, `em_name`, `em_email`, `em_phone`, `em_insurance`, `em_insurance_no`, `em_company_phone`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Rahat Hossain', 1, 'rahat@gmail.com', '01878909871', '01878909872', 'Mirpur-11, Dhaka', 1, 12, 'rahat-1577770245.jpg', '22344566322488', '12-12-1986', '04-02-2025', '31-12-2019', 'Mousumi Islam', 'mousumi@gmail.com', '01982456789', 'Alico', '123245262672', '01987654342', 1, '2019-12-30 23:30:45', '2019-12-30 23:30:45'),
(2, 'Rakbir Hasan', 3, 'rakbir@gmail.com', '01911005555', '01911005556', 'Gazipur Sadar, Gazipur', 2, 21, 'rakbir hasan-1577774898.jpg', '253346546447456877', '03-07-1984', '21-07-2025', '31-12-2019', 'Mousumi Islam', 'mousumi@gmail.com', '01982456789', 'Alico', '123245262644', '01987654345', 1, '2019-12-31 00:48:18', '2019-12-31 00:48:18'),
(3, 'Ahmed', 1, 'admin@gmail.com', '01852478541', NULL, 'SHASAN GASA', 1, 12, NULL, '41898498', '04-02-2020', '11-02-2020', '03-02-2020', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-02-03 01:19:25', '2020-02-03 01:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `company_infos`
--

CREATE TABLE `company_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagline` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_infos`
--

INSERT INTO `company_infos` (`id`, `name`, `tagline`, `description`, `email`, `email2`, `phone`, `phone2`, `logo`, `address`, `website`, `start_date`, `created_at`, `updated_at`) VALUES
(1, 'M. Setara Trade International', 'Gov. Approved Hajj License No. 914', 'M. Setara Trade International is a leading Travel Agency firm in Bangladesh. Our aspiration is to produce high quality, cost effective, reliable result-oriented web and e-commerce solutions on time. Our young and experienced Professionals are here to provide utmost return on your investment in shortest possible time with their talent and proficiency. Our company develops distinctive web solutions which guarantee competitive advantage and improved effectiveness for your business and thus to your end users.', 'selimazadi@yahoo.com', 'selimazadi@msetaratradeinternational.com', '029515646', '01965618947', 'logo-1579675970.png', 'K.R Plaza (11th Floor), 31 Purana Paltan, Dhaka-1000, Bangladesh.', 'http://msetaratradeinternational.com', '01-12-2013', NULL, '2020-01-23 12:06:23');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `flag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `description`, `flag`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Bangladesh', 'South Asian Country', 'bangladesh-1577692695.png', 1, '2019-12-30 01:58:15', '2019-12-30 01:58:15'),
(2, 'India', 'South Asian Country', 'Flag-India-1577692712.jpg', 1, '2019-12-30 01:58:32', '2019-12-30 01:58:32'),
(3, 'Bhutan', 'South Asian Country', 'Bhutan-1577692734.jpg', 1, '2019-12-30 01:58:54', '2019-12-30 01:58:54'),
(4, 'Nepal', 'South Asian Country', 'Flag-of-Nepal-1577692753.png', 1, '2019-12-30 01:59:13', '2019-12-30 01:59:13'),
(5, 'Myanmar', 'South Asian Country', 'FIN-MYA-1577692772.jpg', 1, '2019-12-30 01:59:32', '2019-12-30 01:59:32'),
(6, 'United States', 'European Country', 'usa-1577692810.png', 1, '2019-12-30 02:00:10', '2019-12-30 02:00:10'),
(7, 'United Kingdom', 'European Country', 'uk-1577692823.png', 1, '2019-12-30 02:00:23', '2019-12-30 02:00:23'),
(8, 'Pakistan', 'South Asian Country', 'pakistan-1577692837.png', 1, '2019-12-30 02:00:37', '2019-12-30 02:00:37');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `expense_id` int(10) UNSIGNED NOT NULL,
  `payment_type` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `ex_date` date NOT NULL,
  `ex_note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT 'paid=1, cancel=0, panding=2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `expense_id`, `payment_type`, `bank_id`, `amount`, `ex_date`, `ex_note`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 0, 1000, '2019-12-24', 'dzfSDzxv', '2020-01-06 01:55:06', '2020-01-06 01:55:06', 1),
(2, 2, 1, 0, 2000, '2020-01-02', 'sdzvsDzvsz', '2020-01-06 01:56:23', '2020-01-06 01:56:23', 1),
(3, 3, 1, 0, 5000, '2020-01-06', 'dfzxvszvc', '2020-01-06 01:56:46', '2020-01-06 01:56:46', 1),
(6, 18, 0, 0, 12000, '0000-00-00', NULL, '2020-02-12 02:31:53', '2020-02-12 02:31:53', 2),
(7, 18, 0, 0, 9000, '0000-00-00', NULL, '2020-02-12 02:36:50', '2020-02-12 02:36:50', 2),
(12, 18, 1, 0, 9000, '0000-00-00', NULL, '2020-02-12 04:12:48', '2020-02-12 04:12:48', 2),
(13, 18, 1, 0, 18000, '0000-00-00', NULL, '2020-02-15 01:29:18', '2020-02-15 01:29:18', 2),
(14, 18, 1, 0, 60000, '0000-00-00', NULL, '2020-02-15 01:29:58', '2020-02-15 01:29:58', 2),
(15, 18, 1, 0, 5700, '0000-00-00', NULL, '2020-02-15 01:31:09', '2020-02-15 01:31:09', 2),
(16, 18, 0, 2, 19400, '0000-00-00', NULL, '2020-02-15 01:32:13', '2020-02-15 01:32:13', 2),
(17, 18, 1, 0, 36000, '0000-00-00', NULL, '2020-02-15 01:33:00', '2020-02-15 01:33:00', 2),
(18, 18, 1, 0, 40000, '0000-00-00', NULL, '2020-02-15 01:37:56', '2020-02-15 01:37:56', 2),
(19, 18, 1, 0, 3600, '0000-00-00', NULL, '2020-02-15 02:31:41', '2020-02-15 02:31:41', 2);

-- --------------------------------------------------------

--
-- Table structure for table `expense_names`
--

CREATE TABLE `expense_names` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ex_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ex_desc` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expense_names`
--

INSERT INTO `expense_names` (`id`, `ex_name`, `ex_desc`, `status`, `created_at`, `updated_at`, `parent_id`) VALUES
(1, 'Office Expense', 'All kind of office expenses', 1, '2020-01-01 23:01:02', '2020-01-01 23:01:02', 0),
(3, 'Convenience', 'All kind of Convenience', 1, '2020-01-01 23:02:35', '2020-01-01 23:02:35', 0),
(4, 'Mobile Bill', 'All kind of Mobile Bill', 1, '2020-01-01 23:02:55', '2020-01-01 23:02:55', 0),
(5, 'Salary', 'Staff Salary', 1, '2020-01-01 23:03:46', '2020-01-01 23:03:46', 0),
(6, 'Pre-registration', 'Hajj Pre-registration', 1, '2020-01-23 12:10:40', '2020-01-23 12:10:40', 0),
(7, 'Launch Expense', 'some time its happend', 1, '2020-02-05 05:54:02', '2020-02-05 05:54:02', 0),
(9, 'Tour Expense', NULL, 1, '2020-02-08 04:35:27', '2020-02-08 04:35:27', 0),
(11, 'Launch Expense', 'asdfasdfa', 1, '2020-02-08 04:38:37', '2020-02-08 04:38:37', 1),
(12, 'Tour Expense', 'gfsdgsdf', 1, '2020-02-08 04:38:55', '2020-02-08 04:38:55', 3),
(14, 'Tour Expense', 'dfdsasd', 1, '2020-02-08 04:40:06', '2020-02-08 04:40:06', 0),
(15, 'Travels Expense', 'sdaf asdfads asdf', 1, '2020-02-08 04:40:43', '2020-02-08 04:40:43', 0),
(16, 'launch bill', '50000 Taka', 1, '2020-02-08 05:00:06', '2020-02-08 05:00:06', 1),
(17, 'Hand wash', '90 taka', 1, '2020-02-08 05:02:13', '2020-02-08 05:02:13', 1),
(18, 'Agent Commission', 'Agent passenger based commission.', 1, '2020-02-12 00:01:00', '2020-02-12 00:01:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `foreign_offices`
--

CREATE TABLE `foreign_offices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foreign_offices`
--

INSERT INTO `foreign_offices` (`id`, `name`, `email`, `email2`, `phone`, `phone2`, `address`, `country_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ibne Overseas', 'ibne1@overseas.com', 'ibne2@overseas.com', '9084237725887352', '9084237725887353', 'Katmundu, Nepal', 4, 1, '2020-01-18 01:15:56', '2020-01-18 01:34:58'),
(2, 'Rajkahon', 'rajkahon@gmail.com', 'rajkahon2@gmail.com', '9088776366363', '9088776366364', 'Dillihi, India', 2, 1, '2020-01-18 01:35:59', '2020-01-18 01:37:15'),
(3, 'Islamabad Overseas', 'islamabad@gmail.com', 'islamabad1@gmail.com', '094723577237', '094723577238', 'Islamabad, Pakistan', 8, 1, '2020-01-18 01:39:06', '2020-01-18 01:39:06'),
(4, 'Shohan', 'admin@gmail.com', 'shohan2@gmail.com', '01832548751', NULL, 'SHASAN GASA', 3, 1, '2020-02-03 01:26:49', '2020-02-03 01:26:49');

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE `incomes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inv_id` int(10) UNSIGNED DEFAULT NULL,
  `rem_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `incomes`
--

INSERT INTO `incomes` (`id`, `inv_id`, `rem_id`, `pay_amount`, `created_at`, `updated_at`) VALUES
(2, NULL, 3, 300000, NULL, NULL),
(24, 29, NULL, 300000, NULL, NULL),
(25, 30, NULL, 1000000, NULL, NULL),
(27, 32, NULL, 194000, NULL, NULL),
(28, 33, NULL, 600000, NULL, NULL),
(29, 34, NULL, 400000, NULL, NULL),
(30, 35, NULL, 60000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_date` date NOT NULL,
  `client_id` int(10) UNSIGNED DEFAULT NULL,
  `agent_id` int(10) UNSIGNED NOT NULL,
  `regi_type` int(11) NOT NULL DEFAULT '1' COMMENT 'Registration=1, Pre-registration=0',
  `year` int(11) DEFAULT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `visa_type` int(10) UNSIGNED NOT NULL,
  `payment_type` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `cheque_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_total` int(11) NOT NULL,
  `discount_percent` int(11) DEFAULT NULL,
  `grand_total` int(11) NOT NULL,
  `paid_amount` int(11) NOT NULL,
  `due_amount` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `invoice_no`, `invoice_date`, `client_id`, `agent_id`, `regi_type`, `year`, `country_id`, `visa_type`, `payment_type`, `bank_id`, `cheque_no`, `sub_total`, `discount_percent`, `grand_total`, `paid_amount`, `due_amount`, `created_at`, `updated_at`, `note`) VALUES
(29, 'INV-2020-1', '2020-02-15', 1, 1, 1, 2020, 1, 5, 1, 0, NULL, 300000, NULL, 300000, 300000, 0, '2020-02-15 01:29:18', '2020-02-15 01:29:18', 'Full payment'),
(30, 'INV-2020-30', '2020-02-15', 3, 1, 1, 2020, 1, 6, 1, 0, NULL, 1000000, NULL, 1000000, 1000000, 0, '2020-02-15 01:29:58', '2020-02-15 01:29:58', NULL),
(32, 'INV-2020-31', '2020-02-15', 2, 3, 1, 2020, 1, 5, 0, 2, '62918442984', 200000, 3, 194000, 194000, 0, '2020-02-15 01:32:12', '2020-02-15 01:32:12', NULL),
(33, 'INV-2020-33', '2020-02-15', 1, 1, 1, 2020, 1, 6, 1, 0, NULL, 600000, NULL, 600000, 600000, 0, '2020-02-15 01:33:00', '2020-02-15 01:33:00', NULL),
(34, 'INV-2020-34', '2020-02-15', 2, 3, 1, 2020, 1, 4, 1, 0, NULL, 400000, NULL, 400000, 400000, 0, '2020-02-15 01:37:56', '2020-02-15 01:37:56', NULL),
(35, 'INV-2020-35', '2020-02-15', 1, 1, 1, 2020, 1, 4, 1, 0, NULL, 60000, NULL, 60000, 60000, 0, '2020-02-15 02:31:41', '2020-02-15 02:31:41', 'Special Client');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_infos`
--

CREATE TABLE `invoice_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `pack_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_infos`
--

INSERT INTO `invoice_infos` (`id`, `invoice_id`, `pack_id`, `quantity`, `price`, `total_amount`, `remarks`, `created_at`, `updated_at`) VALUES
(33, 29, 12, 3, 100000, 300000, NULL, NULL, NULL),
(34, 30, 13, 5, 200000, 1000000, NULL, NULL, NULL),
(36, 32, 12, 2, 100000, 200000, NULL, NULL, NULL),
(37, 33, 13, 3, 200000, 600000, NULL, NULL, NULL),
(38, 34, 13, 2, 200000, 400000, NULL, NULL, NULL),
(39, 35, 11, 3, 20000, 60000, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `parent_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 0, 'Dhaka', '2019-12-30 02:10:12', '2019-12-30 02:10:12'),
(2, 0, 'Gazipur', '2019-12-30 02:10:22', '2019-12-30 02:10:22'),
(3, 0, 'Rajbari', '2019-12-30 02:10:29', '2019-12-30 02:10:29'),
(4, 0, 'Gopalgonj', '2019-12-30 02:10:38', '2019-12-30 02:10:38'),
(5, 0, 'Faridpur', '2019-12-30 02:10:44', '2019-12-30 02:10:44'),
(6, 0, 'Tangail', '2019-12-30 02:10:55', '2019-12-30 02:10:55'),
(7, 0, 'Manikgonj', '2019-12-30 02:11:11', '2019-12-30 02:11:11'),
(8, 0, 'Narayangonj', '2019-12-30 02:11:32', '2019-12-30 02:11:32'),
(9, 0, 'Bogura', '2019-12-30 02:11:47', '2019-12-30 02:11:47'),
(10, 0, 'Barisal', '2019-12-30 02:11:58', '2019-12-30 02:11:58'),
(11, 0, 'Borguna', '2019-12-30 02:12:04', '2019-12-30 02:12:04'),
(12, 1, 'Mirpur', '2019-12-30 02:12:13', '2019-12-30 02:12:13'),
(13, 1, 'Kafrul', '2019-12-30 02:12:21', '2019-12-30 02:12:21'),
(14, 1, 'Motijheel', '2019-12-30 02:12:30', '2019-12-30 02:12:30'),
(15, 1, 'Pallabi', '2019-12-30 02:12:36', '2019-12-30 02:12:36'),
(16, 1, 'Paltan', '2019-12-30 02:12:43', '2019-12-30 02:12:43'),
(17, 1, 'Sutrapur', '2019-12-30 02:12:49', '2019-12-30 02:12:49'),
(18, 1, 'Savar', '2019-12-30 02:12:56', '2019-12-30 02:12:56'),
(19, 1, 'Dhamrai', '2019-12-30 02:13:09', '2019-12-30 02:13:09'),
(21, 2, 'Gazipur Sadar', '2019-12-30 02:13:39', '2019-12-30 02:13:39'),
(22, 4, 'Gopalgonj Sadar', '2019-12-30 02:13:52', '2019-12-30 02:13:52'),
(23, 4, 'Muksudpur', '2019-12-30 02:14:02', '2019-12-30 02:14:02'),
(24, 4, 'Kashiani', '2019-12-30 02:14:13', '2019-12-30 02:14:13'),
(25, 4, 'Kotalipara', '2019-12-30 02:14:20', '2019-12-30 02:14:20'),
(26, 4, 'Tungipara', '2019-12-30 02:14:32', '2019-12-30 02:14:32'),
(27, 3, 'Rajbari Sadar', '2019-12-30 02:15:04', '2019-12-30 02:15:04'),
(28, 3, 'Pangsha', '2019-12-30 02:15:15', '2019-12-30 02:15:15'),
(29, 5, 'Alphadanga', '2019-12-30 02:15:32', '2019-12-30 02:15:32'),
(30, 5, 'Vanga', '2019-12-30 02:15:40', '2019-12-30 02:15:40'),
(31, 5, 'Modhukhali', '2019-12-30 02:15:48', '2019-12-30 02:15:48'),
(32, 0, 'Madaripur', '2019-12-30 02:16:08', '2019-12-30 02:16:08'),
(33, 32, 'Madaripur Sadar', '2019-12-30 02:16:29', '2019-12-30 02:16:29'),
(34, 32, 'Kalkini', '2019-12-30 02:17:16', '2019-12-30 02:17:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_12_10_053421_create_locations_table', 1),
(4, '2019_12_12_035242_create_clients_table', 1),
(5, '2019_12_15_094239_create_countries_table', 1),
(6, '2019_12_17_094808_create_visa_types_table', 1),
(7, '2019_12_18_060939_create_payment_types_table', 1),
(8, '2019_12_18_061503_create_company_infos_table', 1),
(10, '2019_12_22_064811_create_packages_table', 1),
(12, '2019_12_22_082751_create_invoice_infos_table', 1),
(13, '2019_12_23_042642_create_banks_table', 1),
(17, '2020_01_02_044447_create_expense_names_table', 3),
(21, '2019_12_22_082710_create_invoices_table', 4),
(22, '2020_01_02_051402_create_expenses_table', 4),
(24, '2019_12_31_042752_create_agents_table', 5),
(26, '2020_01_18_053630_create_foreign_offices_table', 6),
(28, '2020_01_18_090636_create_remittances_table', 7),
(29, '2020_01_18_083441_create_incomes_table', 8),
(30, '2019_12_18_093255_create_visa_apps_table', 9),
(31, '2020_02_04_094751_create_profiles_table', 10),
(32, '2020_02_05_112229_add_parent_id_to_expense_names', 11),
(33, '2020_02_09_060749_add_note_to_invoices', 12),
(34, '2020_02_12_073225_add_commission_to_expenses', 13),
(35, '2020_02_12_073442_add_status_to_expenses', 13);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pack_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pack_desc` text COLLATE utf8mb4_unicode_ci,
  `pack_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `pack_name`, `pack_desc`, `pack_amount`, `status`, `created_at`, `updated_at`) VALUES
(10, 'Visa of Hajj', 'Visa application of Saudi Arabia for Hajj', '500000', 1, '2020-02-15 01:22:47', '2020-02-15 01:22:47'),
(11, 'Visa of Umrah', 'Visa application of Saudi Arabia for Umrah', '20000', 1, '2020-02-15 01:23:13', '2020-02-15 01:23:13'),
(12, 'Visa of Hajj (Full Package)', 'Visa application of Saudi Arabia for Hajj (Full Package)', '100000', 1, '2020-02-15 01:23:56', '2020-02-15 01:23:56'),
(13, 'Visa of HajjVisa of Umrah (Full Package)', 'Visa application of Saudi Arabia for Umrah(Full Package)', '200000', 1, '2020-02-15 01:24:37', '2020-02-15 01:24:37');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'BDT', 'Bangladeshi Taka', 1, '2019-12-30 01:54:23', '2019-12-30 01:54:23'),
(2, 'USD', 'United States Dollar', 1, '2019-12-30 01:54:46', '2019-12-30 01:54:46'),
(3, 'Euro', 'European Currency', 1, '2019-12-30 01:55:06', '2019-12-30 01:55:06'),
(4, 'Rupees', 'Indian Rupees', 1, '2019-12-30 01:55:28', '2019-12-30 01:55:28');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `tag`, `address`, `email`, `phone`, `phone2`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'M. Setera', 'Best service provider', 'SHASAN GASA', 'admin@gmail.com', '01523654125', '01863254785', 'fsdf-1581749848.png', '2020-02-15 00:57:28', '2020-02-15 00:57:28');

-- --------------------------------------------------------

--
-- Table structure for table `remittances`
--

CREATE TABLE `remittances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `office_id` int(10) UNSIGNED NOT NULL,
  `rem_amount` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `remittances`
--

INSERT INTO `remittances` (`id`, `office_id`, `rem_amount`, `note`, `created_at`, `updated_at`) VALUES
(1, 2, 20000, 'Foreign Remittance', '2020-01-18 03:45:45', '2020-01-18 03:45:45'),
(3, 1, 300000, 'Foreign Remittance', '2020-01-18 03:51:36', '2020-01-18 03:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'admin@admin.com', NULL, '$2y$10$9hTZ4CMw7HbH324cbknJWOuh3lF4ynslZfW/HrAhVXqHXFpnDzWQ2', 'oOV0Skdvx6liy0faNlFsgv9nuRiiB4SmqkV1s0QWqB0oRRHwoW5O1yc6NRGV', '2019-12-30 01:45:10', '2019-12-30 01:45:10'),
(2, 'Ahmed', 'admin@gmail.com', NULL, '$2y$10$nZ5Pt1WPR8FuN1qrZFxheeUPvsodqD/5L6GQfsYXy4/eWarQp80p6', NULL, '2020-02-02 13:53:38', '2020-02-02 13:53:38'),
(3, 'Ahmed', 'admin00@gmail.com', NULL, '$2y$10$bej/hw4jjSa2ToHqgV7cBegY1OtXJMNL8CrgAdT5EMVnCCrz1UOR.', NULL, '2020-02-02 04:10:11', '2020-02-02 04:10:11');

-- --------------------------------------------------------

--
-- Table structure for table `visa_apps`
--

CREATE TABLE `visa_apps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `visa_type` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `visa_duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visa_duration_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processing_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `down_payment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `down_payment_type` int(10) UNSIGNED DEFAULT NULL,
  `app_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_fee_type` int(10) UNSIGNED DEFAULT NULL,
  `document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'New=1, Pending=0, Approved=2, Rejected=3',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visa_apps`
--

INSERT INTO `visa_apps` (`id`, `client_id`, `visa_type`, `country_id`, `visa_duration`, `visa_duration_type`, `app_date`, `processing_time`, `down_payment`, `down_payment_type`, `app_fee`, `app_fee_type`, `document`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, '2', 'Days', '29-01-2020', '20', '1000', 1, '10000', 1, NULL, NULL, 1, '2020-01-21 03:19:18', '2020-01-21 03:19:18'),
(2, 1, 1, 2, '14', 'Days', '11-02-2020', NULL, '5000000', 1, NULL, 1, NULL, NULL, 1, '2020-02-03 01:20:41', '2020-02-03 01:20:41');

-- --------------------------------------------------------

--
-- Table structure for table `visa_types`
--

CREATE TABLE `visa_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visa_types`
--

INSERT INTO `visa_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Tourist Visa', 'Tour Visa', 1, '2019-12-30 01:56:25', '2019-12-30 01:56:25'),
(4, 'Contractual Visa', 'Contact Visa', 1, '2019-12-30 01:57:07', '2019-12-30 01:57:07'),
(5, 'Hajj Visa', 'Hajj Visa', 1, '2019-12-30 01:57:26', '2019-12-30 01:57:26'),
(6, 'Omrah Visa', 'Omrah Visa', 1, '2019-12-30 01:57:40', '2019-12-30 01:57:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_infos`
--
ALTER TABLE `company_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_names`
--
ALTER TABLE `expense_names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foreign_offices`
--
ALTER TABLE `foreign_offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_infos`
--
ALTER TABLE `invoice_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `remittances`
--
ALTER TABLE `remittances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visa_apps`
--
ALTER TABLE `visa_apps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visa_types`
--
ALTER TABLE `visa_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `company_infos`
--
ALTER TABLE `company_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `expense_names`
--
ALTER TABLE `expense_names`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `foreign_offices`
--
ALTER TABLE `foreign_offices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `invoice_infos`
--
ALTER TABLE `invoice_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `remittances`
--
ALTER TABLE `remittances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `visa_apps`
--
ALTER TABLE `visa_apps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `visa_types`
--
ALTER TABLE `visa_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
