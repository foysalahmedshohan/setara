@extends('master')

@section('content')
    <div class="row">

        <div class="col-12 col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">{{ __('Add Company Profile') }}</h2>

                    @if ($errors->any())
                        <div class="alert alert-warning" role="alert">
                            <strong>Warning! </strong> @if ($errors->count() == 1)
                                {{$errors->first()}}
                            @else
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    @endif

                    <form role="form" class="forms-sample" action="{{route('companyProfile.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">{{ __('Company Name') }} <span class="req-star">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" minlength="5" name="name" class="form-control p-input" id="name" placeholder="Enter Company Name">
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">{{ __('Tag Name') }} <span class="req-star">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" minlength="5" name="tag" class="form-control p-input" id="tag" placeholder="Enter Tag Name">
                                    </div>
                                </div>

                              

                                <div class="form-group row">
                                    <label for="photo" class="col-sm-4 col-form-label">{{ __('Logo') }}</label>
                                    <div class="col-sm-8">
                                        <input type="file" name="photo" id="photo" class="dropify custom-file-input" data-height="80" data-max-file-size="2048kb" />
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">{{ __('Company Address') }} <span class="req-star">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" minlength="5" name="address" class="form-control p-input" id="address" placeholder="Enter Company Address">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch" class="col-sm-4 col-form-label">{{ __('Phone') }} <span class="req-star">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="tel" name="phone" class="form-control p-input" id="phone" placeholder="Enter Phone Name">
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label for="branch" class="col-sm-4 col-form-label">{{ __('Phone 2') }} <span class="req-star">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="tel" name="phone2" class="form-control p-input" id="phone2" placeholder="Enter Phone Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="op_balance" class="col-sm-4 col-form-label">{{ __('Email Address') }}</label>
                                    <div class="col-sm-8">
                                       <input type="email" name="email" class="form-control p-input" id="email" placeholder="Enter Email Address">
                                    </div>
                                </div>


                            </div>
                        </div>



                        <button type="submit" class="btn btn-success mt-4">{{ __('Submit') }}</button>
                    </form>
                </div>
            </div>
        </div>


    </div>
@endsection