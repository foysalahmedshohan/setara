@extends('master')

@section('content')
          <div class="row">

              <div class="col-12 col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title">{{ __('Company Head Office Profile') }}</h2>
                    <div class="row">
                        <div class="col-12">
                            <table id="order-listing" class="table table-striped" style="width:100%;">
                                <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th>Name</th>
                                    <th>Tag</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Phone 2</th>
                                    <th>Logo</th>
                                    
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                               @foreach ($profiles as $profile)
                                <tr>
                                    <td>{{$no++ }}</td>
                                    <td>{{$profile->name}}</td>
                                    <td>{{$profile->tag}}</td>
                                    <td>{{$profile->phone}}</td>
                                    <td>{{$profile->phone2}}</td>
                                    <td>{{$profile->email}}</td>
                                    <td>{{$profile->address}}</td>

                                    <td>@if ($profile->photo)<img src='{{ asset('uploads/bank/'.$profile->photo) }}' width="80">@endif</td> 

                                    <td>
                                <form id="delete-form-{{ $profile->id }}" action="{{ route('companyProfile.destroy',$profile->id) }}" style="display: none;" method="POST">
                           {{csrf_field()}}company
                           {{ method_field('DELETE') }}
                       </form>
                       <button type="button" class="btn-md btn-outline-danger" title="Delete" onclick="if(confirm('Are you sure? You want to delete this?')){
                           event.preventDefault();
                           document.getElementById('delete-form-{{ $profile->id }}').submit();
                       }else {
                           event.preventDefault();
                               }"><i class="fa fa-trash-o"></i>
                       </button> 
                                    </td>

                                    </tr>
                                     @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
              </div>
              </div>


          </div>
@endsection