@extends('master')

@section('content')
          <div class="row">

              <div class="col-12 col-lg-6 grid-margin">
                  <div class="card">
                      <div class="card-body">
                          <h2 class="card-title">{{ __('Update Your Package') }}</h2>

                          @if ($errors->any())
                              <div class="alert alert-warning alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  @if ($errors->count() == 1)
                                      {{$errors->first()}}
                                  @else
                                      <ul>
                                          @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                          @endforeach
                                      </ul>
                                  @endif
                              </div>
                          @endif

                          <form role="form" class="forms-sample" action="{{route('package.update',[$package->id])}}" method="POST">
                              @csrf
                              <div class="form-group row">
                                  <label for="pack_name" class="col-sm-4 col-form-label">{{ __('Name') }} <span class="req-star">*</span></label>
                                  <div class="col-sm-8">
                                    <input type="text" name="pack_name" value="{{$package->pack_name}}" class="form-control p-input" id="pack_name" placeholder="Enter Name *" required>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label for="pack_desc" class="col-sm-4 col-form-label">{{ __('Description') }}</label>
                                  <div class="col-sm-8">
                                      <textarea name="pack_desc" id="pack_desc" class="form-control p-input" rows="3" placeholder="Enter Description">{{  $package->pack_desc }} </textarea>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label for="pack_amount" class="col-sm-4 col-form-label">{{ __('Price') }} <span class="req-star">*</span></label>
                                  <div class="col-sm-8">
                                      <input type="text" name="pack_amount" value="{{$package->pack_amount}}"
                                       class="form-control p-input" id="pack_amount" placeholder="Enter Price *" required>
                                  </div>
                              </div>

                              <div class="form-group row">
                                    <label for="year" class="col-sm-4 col-form-label">{{ __('Year') }} <span class="req-star">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="year" value="{{$package->pack_year}}" class="form-control p-input" id="year" placeholder="Enter package Year">
                                    </div>
                                </div>

                              <button type="submit" class="btn btn-success mt-4">{{ __('Submit') }}</button>
                          </form>
                      </div>
                  </div>
              </div>
            


          </div>
@endsection