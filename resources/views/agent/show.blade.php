@extends('master')

@section('content')
    <div class="row user-profile">
        <div class="col-lg-4 side-left">
            <div class="card mb-4">
                <div class="card-body avatar">
                    <h2 class="card-title">Info</h2>
                    @if($agent->agent_photo) <img src="{{ asset('uploads/agents/'.$agent->agent_photo) }}" alt="Client Photo" width="47"> @else
                    <img src="http://via.placeholder.com/47x47" alt="Client Photo">@endif
                    <p class="name">{{ $agent->name }}</p>
                    <p class="designation">Agent:  {{$agent->name}}</p>
                    <a class="email" href="#">{{ $agent->email }}</a>
                    <a class="number" href="#">{{ $agent->phone }}</a>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-body overview">
                    <ul class="achivements">
                        <li><p> {{$agent->commission}}%</p><p>Comission</p></li>
                        @foreach($value as $value)
                        @if( $agent->id == $value->agent_id)
                        <li><p>৳{{ ($value->sum/100)*$agent->commission }}</p><p>Total Commission</p></li>
                        @endif
                        @endforeach

                        <li><p>{{$count}}</p><p>Total Client</p></li>
                    </ul>
                  
                    <div class="wrapper about-user">
                        <h2 class="card-title mt-4 mb-3">Location: {{ $agent->address }}</h2>
                        <p></p>
                    </div>
               
                    <div class="info-links">
                       <p>Phone 2: {{ $agent->phone2 }} </p>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 side-right">
            <div class="card">
                <div class="card-body">
                    <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
                        <h2 class="card-title">Details</h2>
                        <ul class="nav nav-pills flex-column flex-sm-row" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Passport Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar">Emergency Contact</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="security-tab" href="">Passenger Edit</a>
                            </li>
                        </ul>
                    </div>
                    <div class="wrapper">
                        <hr>
                        <div class="tab-content" id="myTabContent">

                            <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">

                             
                               <table id="order-listing" class="table table-striped" style="width:100%; float: left;">
                                <thead>
                                <tr>
                                    <th>Invoice No.</th>
                                    <th>Client Name</th>
                                    <th>Grand Total</th>
                                    <th>Paid amount</th>
                                    <th>Commission</th>
                                    <th>Paid</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($invoice as $invoice)
                                <tr>
                                    <td> {{ $invoice->invoice_no }}</td>
                                    <td> {{ $invoice->clients['name'] }}</td>
                                    <td> {{ $invoice->grand_total }}</td>

                                    @if($invoice->paid_amount>0)
                                    <td> {{ $invoice->paid_amount }}
                                    <br><span style="color: blue">Sales</span></td>
                                    @else
                                    <td> {{ $invoice->paid_amount*-1 }}
                                     <br><span style="color: red">Refund</span></td>
                                     @endif
                                     
                                    @if($invoice->paid_amount>0)
                                    <td>{{ ($commission=$invoice->paid_amount/100 )*$invoice->agents['commission'] }}
                                    <br><span style="color: blue"></span></td>
                                    @else
                                    <td>{{ ($commission=-$invoice->paid_amount/100 )*$invoice->agents['commission'] }}
                                    <br><span style="color: red">Cancel</span></td>
                                    @endif
                                    <td><button type="button" class="btn btn-info">+</button></td>

                                   
                                </tr>
                                @endforeach  
                                <h4></h4>
                                </tbody>
                            </table>

                            <div>
                                <h3>Paid Commission</h3>
                                
                            </div>

                          </div>
                         
                              <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                              <h5>invoice No : </h5>
                                <h5>invoice Date : </h5>
                                <h5>client : </h5>
                                <h5>Agent : </h5>
                                <h5>Agent : </h5>
                                <h5>Country : </h5>
                                <h5>Visa Type : </h5>
                            </div>

                            <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                                <h5>Contact Name: </h5>
                                <h5>Email: </h5>
                                <h5>Phone: </h5>
                                <h5>Insurance Company: </h5>
                                <h5>Insurance Policy No: </h5>
                                <h5>Company Phone: </h5>   
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection