@extends('master')

@section('content')

    <div class="row">

        <div class="col-12 col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">{{ __('Add Invoice') }}</h2>
                     <hr>
              
					  <div class="row">
					    <div class="col-sm">
					    <h3>  <b>Invoice No: </b>{{ $invoice->invoice_no }}</h3> 
					     <h3> <b>Invoice Date: </b>{{ $invoice->invoice_date }}</h3> 
					     <h3> <b>Client Name: </b>{{ $invoice->clients['name']  }}</h3> 
					     <h3> <b>Agent Name: </b>{{$invoice->agents['name'] }}</h3> 
					     <h3> <b>Note: </b>{{$invoice->note  }}</h3> 
					     
					      
					    </div>
					    <div class="col-sm">
					     <h3> <b>Year: </b>{{ $invoice->year }}</h3> 
					     <h3> <b>Country: </b>{{ $invoice->country['name'] }}</h3> 
					     <h3> <b>Visa Type: </b>{{ $invoice->visa_types['name'] }}</h3> 
					     <h3> <b>Payment Type:</b>{{ $invoice->payment_types['name'] }}</h3> 
					     <h3> <b>Grand Total:</b>{{ $invoice->grand_total }}</h3> 
					    </div>
					     <div class="col-sm">
					    <h3> <b>Registration Type: </b>{{ 1 }}</h3> 
					    <h3>  <b>Paid Amount: </b>{{ $invoice->paid_amount }}</h3> 
					    <h3>  <b>Due Amount: </b>{{ $invoice->due_amount }}</h3> 
					    
					     <h3> <b>Created Date: </b>{{ $invoice->created_at  }}</h3> 
					    </div>
					    
					  </div>
					  <hr>
					

                    @if ($errors->any())
                        <div class="alert alert-warning" role="alert">
                            <strong>Warning! </strong> @if ($errors->count() == 1)
                                {{$errors->first()}}
                            @else
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    @endif

                    <form role="form" class="forms-sample" action="{{route('invoice.refund_store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                 <div class="form-group row">
                                    <label for="invoice_no" class="col-sm-4 col-form-label">{{ __('Refund Amount:') }} <span class="req-star">*</span></label>
                                   <div class="col-sm-8"> 
                                  <input type="number" class="form-control text-left paid_amount" value="" id="paid_amount" name="paid_amount" placeholder="Enter your paid amount ">
                                </div>
                                 </div>

                                  <div class="form-group row">
                                    <label for="invoice_no" class="col-sm-4 col-form-label">{{ __('Note:') }} <span class="req-star">*</span></label>
                                   <div class="col-sm-8"> 
                                  <textarea name="note" id="note" class="form-control p-input" rows="3" placeholder="Enter Description"></textarea>
                                  </div>

                                 </div>

                                 <button type="submit" class="btn btn-success mt-4">{{ __('Submit') }}</button> 
                                </div> 


                                <div class="col-lg-2 col-12">
                                 <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="invoice_no" value="{{$invoiceno}}" class="form-control p-input" id="invoice_no" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="invoice_date" value="{{$invoice->invoice_date}}" class="form-control p-input" id="invoice_date" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>
                                   <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="agent_id" value="{{$invoice->agent_id}}" class="form-control p-input" id="agent_id" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>
                                    <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="regi_type" value="{{$invoice->regi_type}}" class="form-control p-input" id="regi_type" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>
                                   <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="sub_total" value="{{$invoice->sub_total}}" class="form-control p-input" id="sub_total" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>
                                 </div>





                                  <div class="col-lg-2 col-12">
                                 <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="year" value="{{ $invoice->year }}" class="form-control p-input" id="year" placeholder="" readonly>
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="grand_total" value="{{$invoice->grand_total}}" class="form-control p-input" id="grand_total" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="country_id" value="{{$invoice->country_id}}" class="form-control p-input" id="country_id" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>
                                   <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="invoice_date" value="{{$invoice->invoice_date}}" class="form-control p-input" id="invoice_date" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>


                                    <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="visa_type" value="{{$invoice->visa_type}}" class="form-control p-input" id="visa_type" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>
                                 </div>

                            <div class="col-lg-2 col-12">   
                             <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="client_id" value="{{ $invoice->client_id}}" class="form-control p-input" id="client_id" placeholder="" readonly>
                                    </div>
                                </div>
                                  <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="payment_type" value="{{$invoice->payment_type}}" class="form-control p-input" id="payment_type" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>

                                    <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="cheque_no" value="{{$invoice->cheque_no}}" class="form-control p-input" id="cheque_no" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>
                                 <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="bank_id" value="{{$invoice->bank_id}}" class="form-control p-input" id="bank_id" placeholder="Enter Invoice Number" readonly>
                                    </div>
                                  </div>
                                   <div class="col-sm-12">
                                        <input type="hidden" name="due_amount" value="{{$invoice->due_amount}}" class="form-control p-input" id="due_amount" placeholder="Enter Invoice Number" readonly>
                                    </div>

                            </div>

                        </div>


                    </form>
                </div>
            </div>
        </div>


    </div>
    <script src="{{ asset('assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
   
    <script>
        function bank_paymet(val){
            if(val==0){
                document.getElementById('bankID').style.display = 'block';
                document.getElementById('chequeID').style.display = 'block';
                document.getElementById('bank_id').setAttribute("required", true);
            }else{
                document.getElementById('bankID').style.display = 'none';
                document.getElementById('chequeID').style.display = 'none';
                document.getElementById('bank_id').removeAttribute("required");
            }

         //   document.getElementById('bankID').style.display = style;
        }


        $('tbody').delegate('.quantity, .price, .total_amount','keyup', function(){
            var tr = $(this).parent().parent();
            var quantity = tr.find('.quantity').val();
            var price = tr.find('.price').val();
            var total_amount = (quantity * price);
            tr.find('.total_amount').val(total_amount);

            subtotal();
        });
        function subtotal () {
            var subtotal = 0;
            $('.total_amount').each(function (i,e) {
                var total_p = $(this).val()-0;
                subtotal += total_p;
            });
            $('.sub_total').val(subtotal);
            $('.grand_total').val(subtotal);
        }

        $('.invoice_discount').keyup(function () {
            //   var totaldis = 0;
            var invdis = $(this).val();
            var subt = $('.sub_total').val();
            if (invdis) {
                $('.grand_total').empty();
                var totaldis = (subt/100 * invdis);
                var gtotal = subt - totaldis;
                $('.grand_total').val(gtotal);
            }

        });
        $('.paid_amount').keyup(function () {
            var paidamount = $('.paid_amount').val();
            var gttal = $('.grand_total').val();
            var dueamount = gttal - paidamount;
            var changeamount = paidamount - gttal;
            if (gttal > paidamount ) {
                $('.due_amount').empty();
                $('.due_amount').val(dueamount);
            }else {
                $('.due_amount').empty();
                $('.due_amount').val('0');
            }

        });


        // Package Price
        $('tbody').delegate('.pack_id','change', function(){
            var tr = $(this).parent().parent();
            var id = tr.find('#pack_id :selected').val();
            var dataID = {'id':id};

            $.ajax({
                type:"GET",
                url:"{{url('/invoice/unitPrice')}}",
                dataType: 'json',
                data: dataID,
                success:function (data) {
                    tr.find('.price').val(data.pack_amount);

                }

            });
        });


    </script>
    <script type="text/javascript">
     /*   $('#client_id').change(function(){
            var id = $('#client_id').val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:" ",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        $('#agent_id').val(data.agent_id);
                    }
                });
        });*/

        $('#agent_id').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('invoice/clientID')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('client_id').removeAttribute("disabled");
                            $("#client_id").empty();
                            $("#client_id").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#client_id").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#client_id").empty();
                        }
                    }
                });

        });


    </script>
<script>
    var today = new Date();

    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    // today = yyyy + '/' + mm + '/' + dd;
    today = dd + '-' + mm + '-' + yyyy;

    //    console.log(today);
    document.getElementById('invoice_date').value = today;
</script>

@endsection