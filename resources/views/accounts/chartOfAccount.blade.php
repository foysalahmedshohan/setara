@extends('master')

@section('content')

    <div class="row">

        <div class="col-12 col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">{{ __('Chart Of Account') }}</h2>

                    @if ($errors->any())
                        <div class="alert alert-warning" role="alert">
                            <strong>Warning! </strong> @if ($errors->count() == 1)
                                {{$errors->first()}}
                            @else
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    @endif

                    <form role="form" class="forms-sample" action="{{route('chartOfAccount.add')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <div class="form-group row">
                                    <label for="rootID" class="col-sm-4 col-form-label">{{ __('Root Account') }} <span class="req-star">*</span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control rootId" id="rootId" name="rootId" style="width:100%" >
                                            <option>Select Root Account</option>
                                          @foreach($root as $root)
                                                <option value="{{$root->id}}">
                                                {{$root->title}}</option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>

                                  <div class="form-group row">
                                    <label for="parentId" class="col-sm-4 col-form-label">{{ __('Parent Account') }} <span class="req-star"></span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control parentId" id="parentId" name="parentId" style="width:100%" disabled>
                                          

                                        </select>
                                    </div>
                                  </div>


                                  <div class="form-group row">
                                    <label for="child_id" class="col-sm-4 col-form-label">{{ __('Child Account') }} <span class="req-star"></span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control  child_id" id="child_id" name="child_id" style="width:100%" disabled>
                                            
                                               <!-- <option>Select Agent</option> -->
                                           
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                   <!--  <label for="amount" class="col-sm-4 col-form-label">{{ __('Account Code') }} <span class="req-star">*</span></label> -->
                                    <div class="col-sm-8">
                                        <input type="hidden" name="amount" class="form-control p-input" id="amount" readonly>
                                    </div>
                                </div>


                                   <div class="form-group row">
                                    <label for="title" class="col-sm-4 col-form-label">{{ __('Account Head') }} <span class="req-star">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="title" class="form-control p-input" id="title" placeholder="Enter Account Head">
                                    </div>
                                </div>


                            


                            </div>
                            <div class="col-lg-6 col-12">
  
                            </div>
                            <button type="submit" class="btn btn-success mt-4">{{ __('Submit') }}</button>


                        </div>





                    </form>
                </div>
            </div>
        </div>


    </div>
    <script src="{{ asset('assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
   
    <script>
        function bank_paymet(val){
            if(val==0){
                document.getElementById('bankID').style.display = 'block';
                document.getElementById('bank_id').setAttribute("required", true);
            }else{
                document.getElementById('bankID').style.display = 'none';
                document.getElementById('bank_id').removeAttribute("required");
            }

         //   document.getElementById('bankID').style.display = style;
        }

    </script>


 <script type="text/javascript">
        $('#rootId').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('accounts/parentID')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('parentId').removeAttribute("disabled");
                            $("#parentId").empty();
                            $("#parentId").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#parentId").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#parentId").empty();
                        }
                    }
                });
        });

    </script>



     <script type="text/javascript">
        $('#parentId').change(function(){
            var id = $(this).val();
            var dataID = {'id':id};
                $.ajax({
                    type:"GET",
                    url:"{{url('accounts/parentID')}}",
                    dataType: 'json',
                    data: dataID,
                    success:function(data){
                        if(data){
                            document.getElementById('child_id').removeAttribute("disabled");
                            $("#child_id").empty();
                            $("#child_id").append('<option>Select</option>');
                            $.each(data,function(key,value){
                                $("#child_id").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#child_id").empty();
                        }
                    }
                });
        });

    </script>




@endsection